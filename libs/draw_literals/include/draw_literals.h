#include <ncurses.h>
#include <iostream>
#include <vector>
#include <string>
#include <array>

//#include "../../font/font.h"



namespace draw_stuff {
  class Draw_literals {
  private:
    WINDOW *win;
    //int n = 0;
    int _x = 1;
    bool _show_hours = true;
    static std::string timer_font[11];

    /*
    draw givin string into box and increase x
    */
    void _draw_literal(double numeral);
    void _draw_box();
    // get the unicode strlen to increase x correctly
    size_t _count_utf8_code_points(const char *s);

  public:
    // initialize window and read font
    Draw_literals ();
    // divide int's into numerals
    // draw formatted
    // redraw box
    // set x=1
    void draw_time(double hours, double minutes, double seconds);
    void set_show_hours(bool show_hours);
    void init_window();
    void end();
  };
}
