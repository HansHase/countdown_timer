#include "draw_literals.h"


namespace draw_stuff {
  Draw_literals::Draw_literals () {
    setlocale(LC_ALL, "");

    std::size_t start = 0;
    std::size_t end = 0;
    std::string tmp;
    int numeral = 0;


    for(int i=0; i < 5; ++i) {
  		//mvwprintw(win, i+1, _x, tmp.c_str());

      end = timer_font[numeral].find("\n", start);
      tmp = timer_font[numeral].substr(start, end);
      std::cout << "start: " << start << " end: " << end << "\n";
      std::cout << tmp << "\n";
      start = end + 1;

  	}
    tmp = timer_font[numeral].substr(start, std::string::npos);
    std::cout << "start: " << start << "len: " << tmp.length() << "\n";
    std::cout << tmp << "\n";
  }
  void Draw_literals::set_show_hours(bool show_hours) {
    _show_hours = show_hours;
  }
  void Draw_literals::init_window() {
    initscr();
    if (_show_hours) {
      win = newwin(8,82,10,10);
    }
    else {
      win = newwin(8,60,10,10);
    }
  }
  void Draw_literals::end() {
    endwin();
  }


  void Draw_literals::draw_time(double hours, double minutes, double seconds) {
    double zehner;
    double einer;
    double colon = 10;

    if (_show_hours) {
      zehner = (int)hours/10;
      einer = (int)hours%10;
      _draw_literal(zehner);
      _draw_literal(einer);
      _draw_literal(colon);
    }
    zehner = (int)minutes/10;
    einer = (int)minutes%10;
    _draw_literal(zehner);
    _draw_literal(einer);
    _draw_literal(colon);

    zehner = (int)seconds/10;
    einer = (int)seconds%10;
    _draw_literal(zehner);
    _draw_literal(einer);
    _draw_literal(colon);

    _draw_box();
    _x=1;
  }

  void Draw_literals::_draw_literal(double numeral_d) {
    std::size_t start;
    std::size_t end;
    std::string tmp;
    int numeral = (int) numeral_d;

    end = timer_font[numeral].find("\n");
    tmp = timer_font[numeral].substr(0, end);
    start = end;
    ++start;

    for(int i=0; i < 6; ++i) {
  		mvwprintw(win, i+1, _x, tmp.c_str());

      end = timer_font[numeral].find("\n", start);
      tmp = timer_font[numeral].substr(start, end);
      start = end;
      ++start;

      //std::cout << "start: " << start << "end: " << end << "\n";
  	}
    start = 0;

  	// refresh (redraw / flush) to avoid trouble
  	wrefresh(win);

  	// calculate column offset from timer_font "overhead"
  	_x += _count_utf8_code_points(tmp.c_str());
    //std::cout << "_x" << _x << "\n";
    //std::cout << _x;
  }

  size_t Draw_literals::_count_utf8_code_points(const char *s) {
    size_t count = 0;
    while (*s) {
        count += (*s++ & 0xC0) != 0x80;
    }
    return count;
  }

  void Draw_literals::_draw_box() {
    box(win, 0,0);
  	mvwprintw(win, 0, 36, "Timer");
  	wrefresh(win);
  }

}
