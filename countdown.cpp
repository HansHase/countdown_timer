/* g++ -Wall -lncursesw -o countdown countdown.cpp
*
*
*
*
*/

#include <iostream>
#include <unistd.h>
#include <ncurses.h>
#include <string>
#include <vector>
#include <bits/stdc++.h>
#include <thread>
/*
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
*/

//#include "font.h"
#include "draw_literals.h"


void wait_1s() {
	sleep(1);
}


int convert_input(std::string& input) {
  int seconds = 0;
  std::string::size_type n;
  n = input.find('h');
  if(n != std::string::npos) {
    int hours = std::stoi(input.substr(n-2, 2));
    seconds += hours*3600;
    std::cout << "hours: " << hours << "\n";
  }
  n = input.find('m');
  if(n != std::string::npos) {
    int minutes = std::stoi(input.substr(n-2, 2));
    seconds += minutes*60;
    std::cout << "minutes: " << minutes << "\n";
  }
  n = input.find('s');
  if(n != std::string::npos) {
    int seconds_raw = std::stoi(input.substr(n-2, 2));
    seconds += seconds_raw;
    std::cout << "seconds_raw: " << seconds_raw << "\n";
  }
  std::cout << "seconds: " << seconds << "\n";
  return seconds;
}


/* linear search for literal in vector
	 returns the starting position of the actual literal
*/
/*int find_literal_in_font (std::string literal) {
	for(int i = 0; i < (int) font.size(); i++){
		if(font[i].compare(literal) == 0){
				return i + 2;
				break;
		}
	}
	return -1;
}

int draw_numeral (WINDOW *win, int n, int x) {
	// cycle through vector for numeral
	// each numeral is 6 lines high
	for(int i=n; i < n+6; i++) {
		mvwprintw(win, (i-n)+1, x, font.at(i).c_str());
	}
	// refresh (redraw / flush) to avoid trouble
	wrefresh(win);

	// calculate column offset from font "overhead"
	x += font.at(n-1).length() + 1;
	return x;
}

void draw_box (WINDOW *win) {
	box(win, 0,0);
	mvwprintw(win, 0, 36, "Timer");
	wrefresh(win);
}
*/

int main() {
  double hours = 0;
  double minutes = 0;
  double seconds = 0;
  int count_seconds;
  int tmp = 0;
  std::string raw_input;
	WINDOW *win;
  std::vector<std::string>::iterator it;
	double zehner;
	double einer;
	int n = 0;
	int x = 1;
	//boost::asio::io_service io;


	draw_stuff::Draw_literals draw_boi;
	draw_boi.set_show_hours(false);

  setlocale(LC_ALL, "");
  std::cout << "Enter the time you want to live" << std::endl;
  std::cin >> raw_input;
  count_seconds = convert_input(raw_input);
  std::cout << "count seconds: " << count_seconds << "\n";
  //std::cout<< font.at(1).length() << "\n";

/*
  initscr();
  int hours_t = (int) count_seconds / 3600;
  if (hours_t > 0) {
    win = newwin(8,82,10,10);
  }
  else {
    win = newwin(8,56,10,10);
  }

  mvwprintw(win, 0, 36, "Timer");
*/

	draw_boi.init_window();
  while (count_seconds-- >= 1) {
		// start seperate thread, that sleeps one second
		// and thus will measure our time
		std::thread timer(wait_1s);
		//boost::asio::deadline_timer t(io, boost::posix_time::seconds(1));

		// calculate diffrent time sectors
		// could probably be done better
    tmp = count_seconds;
    hours = (int) tmp / 3600;
    tmp -= 3600*hours;
    minutes = (int) tmp / 60;
    tmp -= 60*minutes;
    seconds = tmp;
		draw_boi.draw_time(hours, minutes, seconds);
		/*draw_box(win);


    //aus zahlen ziffern kriegen
    //ziffer im vector finden
    // ziffer zeichnen
    // abstand ausrechnen
  if (hours > 0) {
    zehner = (int) hours / 10;
    einer = (int) hours % 10;

		n = find_literal_in_font(std::to_string((int)zehner));
		x = draw_numeral(win, n, x);
		n = find_literal_in_font(std::to_string((int)einer));
		x = draw_numeral(win, n, x);
		// draw ':'
		n = find_literal_in_font({':'});
		x = draw_numeral(win, n, x);
	}

	zehner = (int) minutes / 10;
	einer = (int) minutes % 10;
	n = find_literal_in_font(std::to_string((int)zehner));
	x = draw_numeral(win, n, x);
	n = find_literal_in_font(std::to_string((int)einer));
	x = draw_numeral(win, n, x);
	// draw ':'
	n = find_literal_in_font({':'});
	x = draw_numeral(win, n, x);

	zehner = (int) seconds / 10;
	einer = (int) seconds % 10;
	n = find_literal_in_font(std::to_string((int)zehner));
	x = draw_numeral(win, n, x);
	n = find_literal_in_font(std::to_string((int)einer));
	x = draw_numeral(win, n, x);


	draw_box(win);
	x = 1;
*/
	// wait for timer thread to finish, thus completing to wait the 1s
	timer.join();
	//t.wait();




  }
  draw_boi.end();

}
